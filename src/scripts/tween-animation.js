import enquire from 'enquire.js';

let controller = new ScrollMagic.Controller();

function initAnimations() {
  // hero section text parallax
  new ScrollMagic.Scene({
    triggerElement: '.hero__wrapper',
    triggerHook: 'onEnter',
    duration: '100%'
  })
    .setTween('.hero__content-wrapper', { y: '-15%', ease: Power0.easeNone, delay: -0.5 })
    .addTo(controller);

  const heroImageAnimation = new TimelineMax()
    .fromTo('.hero__images-dots', 3, { y: '-150%' }, { y: '20%', ease: Power0.easeNone })
    .fromTo(
      '.hero__images-ellipse',
      3,
      { y: '-50%' },
      { y: '20%', ease: Power0.easeNone, delay: -1 }
    )
    .fromTo(
      '.hero__images-ellipse-large',
      5,
      { y: '-50%' },
      { y: '0%', ease: Power0.easeNone, delay: -3 }
    );

  // hero section image parallax
  new ScrollMagic.Scene({
    triggerElement: '.hero__images-wrapper',
    triggerHook: 'onEnter',
    delay: -2,
    duration: '100%'
  })
    .setTween(heroImageAnimation)
    .addTo(controller);

  // card section parallax
  new ScrollMagic.Scene({
    triggerElement: '.app-preview',
    triggerHook: 0.4,
    duration: '100%'
  })
    .setTween(
      new TimelineMax()
        .fromTo('.app-preview__title', 1, { y: '0%' }, { y: '-150%', ease: Power0.easeNone })
        .fromTo('.app-preview__image', 1, { y: '0%' }, { y: '-30%', ease: Power0.easeNone }, 0)
    )
    .addTo(controller);

  // metal card parallax
  new ScrollMagic.Scene({
    triggerElement: '.card',
    triggerHook: 0.7,
    duration: '100%'
    // delay: -2
  })
    .setTween(
      new TimelineMax()
        .fromTo('.card-content', 3, { y: '10%' }, { y: '-50%', ease: Power0.easeNone })
        .fromTo('.card-before', 2, { y: '50%' }, { y: '-50%', ease: Power0.easeNone, delay: -3 })
        .fromTo(
          '.card__title-before',
          2,
          { y: '150%' },
          { y: '-50%', ease: Power0.easeNone, delay: -3 }
        )
        .fromTo(
          '.card__title-after',
          2,
          { y: '150%' },
          { y: '-120%', ease: Power0.easeNone, delay: -3 }
        )
    )
    .addTo(controller);

  // footer parallax
  new ScrollMagic.Scene({
    triggerElement: '.footer-subscribe',
    triggerHook: 'onEnter',
    duration: '100%'
  })
    .setTween(
      new TimelineMax()
        .fromTo(
          '.footer-subscribe__card',
          3,
          { y: '-50%' },
          {
            y: '0%',
            ease: Power0.easeNone,
            delay: 0
          }
        )
        .fromTo(
          '.footer-subscribe__card-shadow',
          3,
          { y: '10%' },
          {
            y: '-20%',
            ease: Power0.easeNone,
            delay: 0
          }
        )
    )
    .addTo(controller);
}

enquire.register('screen and (min-width: 767px)', {
  match: () => {
    initAnimations();
  },
  unmatch: () => {
    controller.destroy();
    controller = null;
  }
});
