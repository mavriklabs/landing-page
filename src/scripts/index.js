import enquire from 'enquire.js';

import slidr from './slidr';
import './tween-animation';
// uncomment this line to start polling data from server
// import footerCounter from './footer-counter';

import '../styles/index.scss';

$(document).ready(function init($) {
  // uncomment this line to start polling data from server
  // footerCounter();

  const forms = document.querySelectorAll('.hero__form, .footer-subscribe__form');
  const emailFilter = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  forms.forEach(elem => {
    elem.addEventListener('submit', e => {
      e.preventDefault();
      const emailInput = e.target.querySelector('input');
      const emailButton = e.target.querySelector('.button');
      const emailSuccess = e.target.querySelector('.email-sent');

      let isValid = false;
      if (!emailFilter.test(emailInput.value)) {
        emailInput.classList.add('error');
        isValid = false;
      } else {
        emailInput.classList.remove('error');
        isValid = true;
      }

      if (isValid) {
        // emailSuccess.classList.remove('hidden');
        // emailInput.classList.add('hidden');
        // emailButton.classList.add('hidden');

        // setTimeout(() => {
        //   emailSuccess.classList.add('hidden');
        //   emailInput.classList.remove('hidden');
        //   emailButton.classList.remove('hidden');
        // }, 10000);
      }
    });
  });

  enquire.register('screen and (min-width: 767px)', {
    match: () => {
      slidr('hero-slider', {
        controls: false,
        direction: 'vertical',
        fade: true,
        overflow: true,
        timing: { linear: '0.3s ease-out' },
        transition: 'linear'
      })
        .add('v', ['one', 'two', 'three', 'one'])
        .auto(3000, 'down');
    }
  });

  slidr('text-slider', {
    controls: false,
    direction: 'vertical',
    fade: true,
    overflow: true,
    timing: { linear: '0.3s ease-out' },
    transition: 'linear'
  })
    .add('v', ['one', 'two', 'three', 'four', 'five', 'one'])
    .auto(3000, 'down');

  $('.features__slider').slick({
    prevArrow: $('.features__slider-arrow-prev'),
    nextArrow: $('.features__slider-arrow-next'),
    autoplaySpeed: 300,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          dots: true
        }
      }
    ]
  });

});
