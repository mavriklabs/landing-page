// replace with real server address
// scripts expects integer
const serverAddress = 'http://localhost';
const serverPort = '3000';

let startValue = 7000;

async function getNumber() {
  const response = await fetch(`${serverAddress}:${serverPort}`);
  const responseString = await response.json();
  return responseString;
}

const counterAnimation = (id, start, end, duration) => {
  const obj = document.querySelector(id);
  let current = start;
  const range = end - start;
  let increment;
  switch (true) {
    case end > start:
      increment = 1;
      break;
    case end < start:
      increment = -1;
      break;
    default:
      increment = 0;
  }
  const step = Math.abs(Math.floor(duration / range));
  const timer = setInterval(() => {
    current += increment;
    obj.textContent = current;
    if (current === end) {
      clearInterval(timer);
    }
  }, step);
};

const counter = () => {
  setInterval(async () => {
    const newValue = await getNumber();
    counterAnimation('.footer-subscribe__count', startValue, newValue, 3000);
    startValue = newValue;
  }, 5000);
};

export default counter;
