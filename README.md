### Installation
To install node modules run
```
npm install
```

### Development
To run development server run
```
npm start
```

### Build
To generate static files run
```
npm run build
```

### server
To run server
```
npm run server
```

