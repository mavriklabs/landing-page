const express = require('express');
const cors = require('cors');

const app = express();
const port = 3000;

app.use(cors());

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

app.get('/', (req, res) => res.json(getRandomInt(7000, 7100)));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
